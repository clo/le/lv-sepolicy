# Copyright (c) 2022-2024 Qualcomm Innovation Center, Inc. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted (subject to the limitations in the
# disclaimer below) provided that the following conditions are met:
#
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#
#    * Redistributions in binary form must reproduce the above
#      copyright notice, this list of conditions and the following
#      disclaimer in the documentation and/or other materials provided
#      with the distribution.
#
#    * Neither the name of Qualcomm Innovation Center, Inc. nor the names of its
#      contributors may be used to endorse or promote products derived
#      from this software without specific prior written permission.
#
# NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE
# GRANTED BY THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT
# HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
# GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
# IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#============= init_t ==============
gen_require(`
    type kmod_t;
    type sysadm_t;
    type pulseaudio_t;
    type pulseaudio_conf_t;
    type audio_vendor_t;
    type weston_data_t;
    type weston_lib_t;
    type audio_vendor_exec_t;
    type gstreamer_data_t;
    type gstreamer_lib_t;
')

qti_list_dsp_dirs(init_t)
qti_list_dsp_dirs(initrc_t)

allow init_t initrc_t:process2 nnp_transition;
allow init_t initrc_t:binder { call transfer };
allow init_t kmod_t:process2 nnp_transition;
allow init_t sysadm_t:process transition;
allow init_t audio_vendor_t:file { execute execute_no_trans map open read };

#============= initrc_t ==============
allow initrc_t sysadm_t:system start;
allow initrc_t sysadm_t:binder transfer;
allow initrc_t sysadm_t:process transition;
allow initrc_t bin_t:service { start stop status };
allow initrc_t pulseaudio_t:binder transfer;
allow initrc_t pulseaudio_conf_t:dir { getattr open read search };
allow initrc_t pulseaudio_conf_t:file { read open getattr };
allow initrc_t audio_vendor_t:binder call;
allow initrc_t audio_vendor_t:file { getattr execute read open execute_no_trans map};
allow initrc_t weston_data_t:file { getattr open read };
allow initrc_t weston_lib_t:file { getattr read rename };
allow initrc_t weston_lib_t:lnk_file getattr;
allow initrc_t self:process execmem;
allow initrc_t audio_vendor_exec_t:service { start status stop };
allow initrc_t self:binder { call set_context_mgr transfer };
allow initrc_t self:bpf { map_create map_read map_write prog_load prog_run };
allow initrc_t self:cap_userns { dac_override net_admin net_raw setgid setuid sys_admin sys_ptrace };
allow initrc_t audio_vendor_exec_t:service { start status stop };
allow initrc_t gstreamer_data_t:file getattr;
allow initrc_t gstreamer_lib_t:file getattr;
